FROM python:3.9-slim

COPY . /app
WORKDIR /app

EXPOSE 8891

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
    build-essential curl libssl-dev libffi-dev python3-dev zsh git \
    libbz2-dev libncurses5-dev libncursesw5-dev libreadline-dev \
    libsqlite3-dev liblzma-dev python3-venv && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

RUN sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended

RUN chsh -s /usr/bin/zsh

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"

RUN curl https://pyenv.run | zsh

ENV PATH="/root/.pyenv/bin:/root/.pyenv/shims:${PATH}"
RUN echo 'export PATH="/root/.pyenv/bin:$PATH"' >> /root/.zshrc && \
    echo 'eval "$(pyenv init --path)"' >> /root/.zshrc && \
    echo 'eval "$(pyenv init -)"' >> /root/.zshrc && \
    echo 'eval "$(pyenv virtualenv-init -)"' >> /root/.zshrc

SHELL ["/bin/zsh", "-c"]
RUN source /root/.zshrc && \
    pyenv install 3.9.7 && \
    pyenv virtualenv 3.9.7 ecdbr && \
    pyenv activate ecdbr

RUN source /root/.zshrc && pyenv activate ecdbr && \
    pip install pipx && \
    pipx ensurepath && \
    source /root/.zshrc && \
    pip install jupyter && \
    pip install pandas

RUN source /root/.zshrc && pyenv activate ecdbr && pip install maturin && \
    ipython kernel install --user --name ecdbr

RUN source /root/.zshrc && \
    pyenv activate ecdbr && \
    pip install numpy matplotlib

RUN echo 'source /root/.pyenv/versions/ecdbr/bin/activate' >> /root/.zshrc

CMD ["zsh"]
