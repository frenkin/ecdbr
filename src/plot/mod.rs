/// Module containing [`Plot`]s.
use core::ops::Range;

use pyo3::{
    pyclass, pymethods,
    types::{PyAnyMethods, PyModule},
    Py, Python,
};

use crate::stl::algos::verify::range_intersect;

pub mod operation;

/// A [`Segment`] corresponds to a piece of the piecewise affine function
/// described by the [`Plot`].
#[pyclass]
#[derive(Debug, Clone)]
pub struct Segment {
    /// Interval
    range: Range<f32>,
    /// Slope
    slope: f32,
    /// Intercept
    intercept: f32,
}

#[pymethods]
impl Segment {
    /// Returns the start of the interval associated with the [`Segment`].
    const fn low(&self) -> f32 {
        self.range.start
    }

    /// Returns the end of the interval associated with the [`Segment`].
    const fn high(&self) -> f32 {
        self.range.end
    }

    /// Returns the slope associated with the [`Segment`].
    const fn slope(&self) -> f32 {
        self.slope
    }
    
    /// Returns the intercept associated with the [`Segment`].
    const fn intercept(&self) -> f32 {
        self.intercept
    }
}

impl Segment {
    /// Creates a new [`Segment`].
    #[must_use]
    pub const fn new(range: Range<f32>, slope: f32, intercept: f32) -> Self {
        Self {
            range,
            slope,
            intercept,
        }
    }
}

/// A [`Plot`] is a piecewise affine function.
#[pyclass]
#[derive(Debug, Clone)]
pub struct Plot(Vec<Segment>);

impl Plot {
    /// Creates a new [`Plot`].
    #[must_use]
    pub(crate) fn new(segments: &[Segment]) -> Self {
        let mut result = Vec::new();
        let mut iter = segments.iter().peekable();

        while let Some(seg1) = iter.next() {
            let mut last_high = seg1.high();
            while let Some(&seg_last) = iter.peek() {
                if seg1.intercept() == seg_last.intercept()
                    && seg1.slope() == seg_last.slope()
                {
                    last_high = seg_last.high();
                    iter.next();
                } else {
                    break;
                }
            }
            result.push(Segment::new(
                seg1.low()..last_high,
                seg1.slope,
                seg1.intercept,
            ));
        }

        Self(result)
    }

    /// Returns the minimum value of the function between start and end
    pub fn min(&self, start: f32, end: f32) -> Option<f32> {
        self.0
            .iter()
            .filter(|seg| {
                range_intersect(&(start..end), &(seg.low()..seg.high()))
            })
            .map(|segment| {
                let segment_start = f32::max(segment.range.start, start);
                let segment_end = f32::min(segment.range.end, end);
                let value_at_start =
                    segment.slope.mul_add(segment_start, segment.intercept);
                let value_at_end =
                    segment.slope.mul_add(segment_end, segment.intercept);
                f32::min(value_at_start, value_at_end)
            })
            .min_by(f32::total_cmp)
    }

    /// Returns the maximum value of the function between start and end
    pub fn max(&self, start: f32, end: f32) -> Option<f32> {
        self.0
            .iter()
            .filter(|seg| {
                range_intersect(&(start..end), &(seg.low()..seg.high()))
            })
            .map(|segment| {
                let segment_start = f32::max(segment.range.start, start);
                let segment_end = f32::min(segment.range.end, end);
                let value_at_start =
                    segment.slope.mul_add(segment_start, segment.intercept);
                let value_at_end =
                    segment.slope.mul_add(segment_end, segment.intercept);
                f32::max(value_at_start, value_at_end)
            })
            .max_by(f32::total_cmp)
    }

    /// Removes the negative part of the domain from the plot.
    pub fn remove_negative(self) -> Self {
        Self(
            self.0
                .into_iter()
                .filter_map(|s| {
                    if s.high() > 0. {
                        Some(Segment::new(
                            f32::max(s.low(), 0.)..s.high(),
                            s.slope,
                            s.intercept,
                        ))
                    } else {
                        None
                    }
                })
                .collect(),
        )
    }
}

#[pymethods]
impl Plot {
    /// Returns the value at time `a`.
    ///
    /// If a is not within the limits of the function, then [`None`] is
    /// returned.
    pub(crate) fn value_at(&self, a: f32) -> Option<f32> {
        self.0
            .iter()
            .rev()
            .find(|seg| (seg.low()..=seg.high()).contains(&a))
            .map(|s| {
                let slope = s.slope;
                let intercept = s.intercept;
                slope.mul_add(a, intercept)
            })
    }

    /// Returns the [`Plot`] as a list of [`Segment`]s.
    #[pyo3(name = "segments")]
    fn segments_py(&self) -> Vec<Segment> {
        self.0.clone()
    }

    /// Function for displaying in a Python notebook.
    fn __repr__(s: Py<Self>) -> String {
        Python::with_gil(|py| {
            let functions = PyModule::from_code_bound(
                py,
                include_str!("../aux.py"),
                "aux.py",
                "aux",
            )
            .unwrap();
            let fu = functions.getattr("display_plot").unwrap();
            drop(fu.call1((s,)));
        });
        String::default()
    }

    /// Returns the first and the last moment of the plot.
    fn limits(&self) -> (f32, f32) {
        (self.0.first().unwrap().low(), self.0.last().unwrap().high())
    }
}
