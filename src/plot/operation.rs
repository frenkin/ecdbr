//! Module containing operations on [`Plot`]s
use core::f32;

use pyo3::{pyclass, pymethods};

use crate::plot::Segment;

use super::Plot;

/// All operations on [`Plot`].
#[pyclass]
#[derive(Copy, Clone, Debug)]
pub enum Operation {
    /// Next Value Match
    Next(f32),
    /// Previous Value Match
    Prev(f32),
    /// Current Value Segment
    CurrSeg(f32),
    /// Constant Function
    Const(f32),
    /// Time Dilation
    TDilate(f32),
    /// Space Dilation
    SDilate(f32),
    /// Time Translation
    TTranslate(f32),
    /// Space Translation
    STranslate(f32),
    /// Identity Difference
    IdDiff(),
    /// Maximum
    Max(),
    /// Minimum
    Min(),
    /// Anti-value
    Anti(),
}

#[pymethods]
impl Operation {
    /// Python binding of [`apply`](Self::apply).
    #[pyo3(name = "apply")]
    #[must_use]
    pub fn apply_py(&self, plots: Vec<Plot>) -> Plot {
        self.apply(plots.iter().collect::<Vec<_>>().as_slice())
    }

    /// Creates a new [`Operation`].
    ///
    /// The name used to create an [`Operation`] corresponds to the notation
    /// used in the article.
    #[new]
    #[pyo3(signature = (name, val=None))]
    #[must_use]
    pub fn new(name: &str, val: Option<f32>) -> Self {
        match name.to_lowercase().as_str() {
            "next" => Self::Next(
                val.unwrap_or_else(|| panic!("Next takes one argument")),
            ),
            "prev" => Self::Prev(
                val.unwrap_or_else(|| panic!("Prev takes one argument")),
            ),
            "currseg" => Self::CurrSeg(
                val.unwrap_or_else(|| panic!("CurrSeg takes one argument")),
            ),
            "const" => Self::Const(
                val.unwrap_or_else(|| panic!("Const takes one argument")),
            ),
            "tdilate" => Self::TDilate(
                val.unwrap_or_else(|| panic!("TDilate takes one argument")),
            ),
            "sdilate" => Self::SDilate(
                val.unwrap_or_else(|| panic!("SDilate takes one argument")),
            ),
            "ttranslate" => Self::TTranslate(
                val.unwrap_or_else(|| panic!("TTranslate takes one argument")),
            ),
            "stranslate" => Self::STranslate(
                val.unwrap_or_else(|| panic!("STranslate takes one argument")),
            ),
            "iddiff" => Self::IdDiff(),
            "max" => Self::Max(),
            "min" => Self::Min(),
            "anti" => Self::Anti(),
            _ => unimplemented!(),
        }
    }
}

impl Operation {
    /// Returns the Plot for `CurrSeg`
    fn current_seg(plots: &[&Plot], v: f32) -> Plot {
        let next = {
            assert!(!plots.is_empty(), "CurrSeg takes one argument");
            let f = *plots.first().unwrap();
            // Work on signals
            assert!(f.0.iter().all(|s| s.slope == 0.), "Works on signals");
            let new_segments: Vec<_> =
                f.0.iter()
                    .map(|seg| {
                        if seg.intercept == v {
                            let next = f.0.iter().find(|x| {
                                x.intercept != v && x.low() >= seg.high()
                            });
                            let intercept = next
                                .map_or(f32::INFINITY, |next_seg| {
                                    next_seg.range.start
                                });
                            Segment {
                                range: seg.range.clone(),
                                slope: 0.,
                                intercept,
                            }
                        } else {
                            Segment {
                                range: seg.range.clone(),
                                slope: 1.,
                                intercept: seg.range.start,
                            }
                        }
                    })
                    .collect();

            Plot(new_segments)
        };
        let prev = {
            let f = *plots.first().unwrap();
            assert!(f.0.iter().all(|s| s.slope == 0.), "Works on signals");
            let new_segments: Vec<_> = f
                .0
                .iter()
                .map(|seg| {
                    if seg.intercept == v {
                        let prev =
                            f.0.iter()
                                .filter(|x| {
                                    x.intercept != v && x.high() <= seg.low()
                                })
                                .last();
                        let intercept =
                            prev.map_or(f32::NEG_INFINITY, |s| s.high());
                        Segment {
                            range: seg.range.clone(),
                            slope: 0.,
                            intercept,
                        }
                    } else {
                        Segment {
                            range: seg.range.clone(),
                            slope: 1.,
                            intercept: seg.range.start,
                        }
                    }
                })
                .collect();

            Plot(new_segments)
        };
        assert_eq!(prev.0.len(), next.0.len());
        let mut segs = vec![];
        for i in 0..prev.0.len() {
            let seg_next = &next.0[i];
            let seg_prev = &prev.0[i];
            let intercept = if seg_prev.intercept.is_infinite()
                || seg_next.intercept.is_infinite()
            {
                f32::INFINITY
            } else if seg_prev.slope == 1. {
                seg_next.high() - seg_prev.low()
            } else {
                0.
            };
            let seg = Segment::new(seg_next.range.clone(), 0., intercept);
            segs.push(seg);
        }
        Plot::new(&segs)
    }

    /// Apply an [`Operation`] on a [`Plot`].
    ///
    /// Applying an [`Operation`] means creating a new [`Plot`] representing the
    /// result of the operation on the set of [`Plot`]s given as an argument.
    ///
    /// Special case: Const returns a signal over [-∞, ∞).
    #[must_use]
    pub fn apply(&self, plots: &[&Plot]) -> Plot {
        match *self {
            Self::Next(v) => {
                assert!(!plots.is_empty(), "Next takes one argument");
                let f = *plots.first().unwrap();
                // Work on signals
                assert!(f.0.iter().all(|s| s.slope == 0.), "Works on signals");
                let new_segments: Vec<_> = f
                    .0
                    .iter()
                    .enumerate()
                    .map(|(pos, seg)| {
                        if seg.intercept == v {
                            Segment {
                                range: seg.range.clone(),
                                slope: 1.,
                                intercept: 0.,
                            }
                        } else {
                            let next =
                                f.0.iter().skip(pos).find(|x| x.intercept == v);
                            let intercept = next
                                .map_or(f32::INFINITY, |next_seg| {
                                    next_seg.range.start
                                });
                            Segment {
                                range: seg.range.clone(),
                                slope: 0.,
                                intercept,
                            }
                        }
                    })
                    .collect();

                Plot::new(&new_segments)
            },
            Self::Prev(v) => {
                assert!(!plots.is_empty(), "Prev takes one argument");
                let f = *plots.first().unwrap();
                // Work on signals
                assert!(f.0.iter().all(|s| s.slope == 0.), "Works on signals");
                let new_segments: Vec<_> =
                    f.0.iter()
                        .enumerate()
                        .map(|(pos, seg)| {
                            if seg.intercept == v {
                                Segment {
                                    range: seg.range.clone(),
                                    slope: 1.,
                                    intercept: 0.,
                                }
                            } else {
                                let prev_pos =
                                    f.0.iter()
                                        .take(pos)
                                        .rposition(|x| x.intercept == v);
                                let intercept =
                                    prev_pos.map_or(f32::NEG_INFINITY, |pos| {
                                        let prev_seg = &f.0[pos];
                                        prev_seg.range.end
                                    });
                                Segment {
                                    range: seg.range.clone(),
                                    slope: 0.,
                                    intercept,
                                }
                            }
                        })
                        .collect();

                Plot::new(&new_segments)
            },
            Self::CurrSeg(v) => Self::current_seg(plots, v),
            Self::Const(a) => Plot::new(&[Segment {
                range: f32::NEG_INFINITY..f32::INFINITY,
                slope: 0.,
                intercept: a,
            }]),
            Self::TDilate(a) => {
                assert!(!plots.is_empty(), "TDilate takes one argument");
                let new_segments: Vec<_> = plots
                    .first()
                    .unwrap()
                    .0
                    .iter()
                    .map(|s| {
                        let range = (s.range.start * a)..(s.range.end * a);
                        let slope = s.slope / a;
                        let intercept = s.intercept;
                        Segment {
                            range,
                            slope,
                            intercept,
                        }
                    })
                    .collect();
                Plot::new(&new_segments)
            },
            Self::SDilate(a) => {
                assert!(!plots.is_empty(), "SDilate takes one argument");
                let new_segments: Vec<_> = plots
                    .first()
                    .unwrap()
                    .0
                    .iter()
                    .map(|s| {
                        let range = s.range.clone();
                        let slope = s.slope / a;
                        let intercept = s.intercept / a;
                        Segment {
                            range,
                            slope,
                            intercept,
                        }
                    })
                    .collect();
                Plot::new(&new_segments)
            },
            Self::TTranslate(a) => {
                // Warning: this construction may lead to signals where the
                // first instant is not 0.
                assert!(!plots.is_empty(), "TTranslate takes one argument");
                let new_segments: Vec<_> = plots[0]
                    .0
                    .iter()
                    .map(|s| {
                        let range = (s.range.start + a)..(s.range.end + a);
                        let slope = s.slope;
                        let intercept = s.intercept;
                        Segment {
                            range,
                            slope,
                            intercept,
                        }
                    })
                    .collect();
                Plot::new(&new_segments).remove_negative()
            },
            Self::STranslate(a) => {
                assert!(!plots.is_empty(), "STranslate takes one argument");
                let new_segments: Vec<_> = plots[0]
                    .0
                    .iter()
                    .map(|s| {
                        let range = s.range.clone();
                        let slope = s.slope;
                        let intercept = s.intercept + a;
                        Segment {
                            range,
                            slope,
                            intercept,
                        }
                    })
                    .collect();
                Plot::new(&new_segments)
            },
            Self::IdDiff() => {
                assert!(!plots.is_empty(), "IdDiff takes one argument");
                let new_segments: Vec<_> = plots
                    .first()
                    .unwrap()
                    .0
                    .iter()
                    // .filter(|s| s.intercept.is_finite())
                    .map(|s| {
                        let range = s.range.clone();
                        let intercept = -1. * s.intercept;
                        let slope = 1. - s.slope;
                        Segment {
                            range,
                            slope,
                            intercept,
                        }
                    })
                    .collect();
                Plot::new(&new_segments)
            },
            Self::Max() => {
                assert!(plots.len() > 1, "Max takes two arguments");
                let f1 = plots[0];
                let f2 = plots[1];
                let mut segments = Vec::new();
                let mut i = 0;
                let mut j = 0;

                while i < f1.0.len() && j < f2.0.len() {
                    let seg1 = &f1.0[i];
                    let seg2 = &f2.0[j];

                    let start = seg1.range.start.max(seg2.range.start);
                    let end = seg1.range.end.min(seg2.range.end);

                    if start < end {
                        let slope1 = seg1.slope;
                        let intercept1 = seg1.intercept;
                        let slope2 = seg2.slope;
                        let intercept2 = seg2.intercept;

                        let intersection_x = if slope1 == slope2 {
                            if intercept1 == intercept2 {
                                start
                            } else {
                                f32::INFINITY
                            }
                        } else {
                            (intercept2 - intercept1) / (slope1 - slope2)
                        };
                        // If there is an intersection, add part of both
                        // segments.
                        if start <= intersection_x && intersection_x <= end {
                            // Imposes low_seg as the lowest segment at the
                            // beginning.
                            // This implies that low_seg is the first of the two
                            // and high_seg the second.
                            let f1_val = f1.value_at(start);
                            let f2_val = f2.value_at(start);
                            let (low_seg, high_seg) = if f1_val < f2_val
                                || (f1_val == f2_val && seg1.slope > seg2.slope)
                            {
                                (seg1, seg2)
                            } else {
                                (seg2, seg1)
                            };
                            if start != intersection_x {
                                segments.push(Segment {
                                    range: start..intersection_x,
                                    slope: high_seg.slope,
                                    intercept: high_seg.intercept,
                                });
                            }
                            segments.push(Segment {
                                range: intersection_x..end,
                                slope: low_seg.slope,
                                intercept: low_seg.intercept,
                            });
                        // If there is no intersection, the one with the largest
                        // intersection is the largest over the whole interval.
                        } else if f1.value_at(start) > f2.value_at(start) {
                            segments.push(Segment {
                                range: start..end,
                                slope: slope1,
                                intercept: seg1.intercept,
                            });
                        } else {
                            segments.push(Segment {
                                range: start..end,
                                slope: slope2,
                                intercept: seg2.intercept,
                            });
                        }
                    }

                    if seg1.range.end < seg2.range.end {
                        i += 1;
                    } else {
                        j += 1;
                    }
                }

                Plot::new(&segments).remove_negative()
            },
            Self::Min() => {
                // Construction similar to max.
                assert!(plots.len() > 1, "Min takes two arguments");
                let f1 = plots[0];
                let f2 = plots[1];
                let mut segments = Vec::new();
                let mut i = 0;
                let mut j = 0;

                while i < f1.0.len() && j < f2.0.len() {
                    let seg1 = &f1.0[i];
                    let seg2 = &f2.0[j];

                    let start = seg1.range.start.max(seg2.range.start);
                    let end = seg1.range.end.min(seg2.range.end);

                    if start < end {
                        let slope1 = seg1.slope;
                        let intercept1 = seg1.intercept;
                        let slope2 = seg2.slope;
                        let intercept2 = seg2.intercept;

                        let intersection_x = if slope1 == slope2 {
                            if intercept1 == intercept2 {
                                start
                            } else {
                                f32::INFINITY
                            }
                        } else {
                            (intercept2 - intercept1) / (slope1 - slope2)
                        };
                        // If there is an intersection, add part of both
                        // segments.
                        if start <= intersection_x && intersection_x <= end {
                            // Imposes low_seg as the lowest segment at the
                            // beginning.
                            // This implies that low_seg is the first of the two
                            // and high_seg the second.
                            let f1_val = f1.value_at(start);
                            let f2_val = f2.value_at(start);
                            let (low_seg, high_seg) = if f1_val < f2_val
                                || (f1_val == f2_val && seg1.slope > seg2.slope)
                            {
                                (seg1, seg2)
                            } else {
                                (seg2, seg1)
                            };
                            if start != intersection_x {
                                segments.push(Segment {
                                    range: start..intersection_x,
                                    slope: low_seg.slope,
                                    intercept: low_seg.intercept,
                                });
                            }
                            segments.push(Segment {
                                range: intersection_x..end,
                                slope: high_seg.slope,
                                intercept: high_seg.intercept,
                            });
                        } else if f1.value_at(start) < f2.value_at(start) {
                            segments.push(Segment {
                                range: start..end,
                                slope: slope1,
                                intercept: seg1.intercept,
                            });
                        } else {
                            segments.push(Segment {
                                range: start..end,
                                slope: slope2,
                                intercept: seg2.intercept,
                            });
                        }
                    }

                    if seg1.range.end < seg2.range.end {
                        i += 1;
                    } else {
                        j += 1;
                    }
                }

                Plot::new(&segments).remove_negative()
            },
            Self::Anti() => {
                assert!(!plots.is_empty(), "Anti takes one argument");
                let segments = &plots[0].0;
                let new_segments: Vec<Segment> = segments
                    .iter()
                    .map(|s| {
                        let range = s.range.clone();
                        let slope = -1. * s.slope;
                        let intercept = -1. * s.intercept;
                        Segment {
                            range,
                            slope,
                            intercept,
                        }
                    })
                    .collect();
                Plot::new(&new_segments)
            },
        }
    }
}
