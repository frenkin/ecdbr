//! Segment description module.
use core::ops::Range;

use pyo3::{pyclass, pymethods};

use super::values::IntValues;

#[pyclass]
#[derive(Clone, Debug)]
/// A [`Segment`] is a triplet (start, values, end).
pub struct Segment {
    /// upper and lower limits
    limits: Range<f32>,
    /// Associated values
    values: IntValues,
}

impl Segment {
    /// Returns `Some(self.values)` if time is in the corresponding limits,
    /// `None` otherwise.
    #[must_use]
    pub fn value_at(&self, time: f32) -> Option<IntValues> {
        (self.limits.start <= time && self.limits.end >= time)
            .then_some(self.values)
    }

    /// Creates a new [`Segment`].
    #[must_use]
    pub const fn new(low: f32, values: IntValues, high: f32) -> Self {
        Self {
            limits: low..high,
            values,
        }
    }
}

#[pymethods]
impl Segment {
    /// Returns the start of the range of the [`Segment`].
    #[must_use]
    pub const fn low(&self) -> f32 {
        self.limits.start
    }

    #[must_use]
    /// Returns the end of the range of the [`Segment`].
    pub const fn high(&self) -> f32 {
        self.limits.end
    }

    /// Returns the value contained in the [`Segment`].
    #[must_use]
    pub const fn py_values(&self) -> IntValues {
        self.values
    }

    /// Python constructor.
    #[new]
    #[must_use]
    pub fn new_py(low: f32, values: Vec<u8>, high: f32) -> Self {
        Self {
            limits: low..high,
            values: IntValues::from(values),
        }
    }
}

impl Segment {
    /// Returns a reference to the values of this [`Segment`].
    #[must_use]
    pub const fn values(&self) -> &IntValues {
        &self.values
    }

    #[must_use]
    /// Returns the lower and the lower bounds as a [`Range`].
    pub const fn limits(&self) -> &Range<f32> {
        &self.limits
    }

    /// Returns the dimension of this [`Segment`].
    #[must_use]
    pub const fn dimension(&self) -> usize {
        self.values.len()
    }
}
