//! Module for calculating the Hausdorff distance between two [`RangeUnion`]s.
use core::f32;

use pyo3::pyfunction;

use super::range_sort::RangeUnion;

/// Computes the Hausdorff distance between two [`RangeUnion`]s.
#[pyfunction]
pub fn h_dist(s: &RangeUnion, r: &RangeUnion) -> f32 {
    let n = s.ranges().len();
    let m = r.ranges().len();
    assert!(n > 0 && m > 0, "Signal dimensions must be non-zero");
    let mut l = s.ranges().first().unwrap().start;
    let mut b = Vec::with_capacity(n + 1);
    let mut sp = Vec::with_capacity(n + m);
    b.push(f32::NEG_INFINITY);
    b.extend(r.ranges().windows(2).map(|vals| {
        let left = vals[1].start;
        let right = vals[0].end;
        (left + right) / 2.
    }));
    b.push(f32::INFINITY);
    let mut i = 1;
    let mut j = 1;
    while i <= n {
        while j <= m {
            if b[j] <= l {
                j += 1;
            } else if s.ranges()[i - 1].end <= b[j] {
                let rim1 = &s.ranges()[i - 1];
                sp.push((l, rim1.end, j));
                i += 1;
                l = s
                    .ranges()
                    .get(i - 1)
                    .map_or(f32::INFINITY, |val| val.start);
            } else {
                sp.push((l, b[j], j));
                j += 1;
                l = r.ranges().get(j - 1).map_or(f32::INFINITY, |val| val.end);
            }
        }
    }
    sp.into_iter()
        .map(|(l, u, j)| {
            let rj = &r.ranges()[j - 1];
            f32::max(rj.start - l, u - rj.end)
        })
        .max_by(f32::total_cmp)
        .unwrap_or(0.)
}
