//! Module for calculating distance between signals.
use pyo3::pyfunction;

use crate::signal::Signal;

use super::{
    h_dist::h_dist,
    range_sort::{range_sort, SortedRange},
};

/// Computes the distance between two [`Signal`]s.
#[pyfunction]
pub fn dist(s: &Signal, r: &Signal) -> f32 {
    if s.dimension() == 0 || r.dimension() == 0 {
        0.
    } else {
        let rs = range_sort(s);
        let rr = range_sort(r);
        if SortedRange::same(&rs, &rr) {
            rs.items()
                .iter()
                .map(|ra| {
                    let v = ra.values();
                    let sgs_s = ra.ranges();
                    let sgs_r =
                        rr.items().iter().find(|x| x.values() == v).unwrap();
                    let left = h_dist(sgs_s, sgs_r.ranges());
                    let right = h_dist(sgs_r.ranges(), sgs_s);
                    f32::max(left, right)
                })
                .max_by(f32::total_cmp)
                .unwrap_or(0.)
        } else {
            f32::INFINITY
        }
    }
}
