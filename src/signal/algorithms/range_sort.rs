//! Module of [`range_sort`].
use std::ops::{Add, Range};

use pyo3::{pyclass, pyfunction, pymethods};

use crate::signal::{segment::Segment, values::IntValues, Signal};

/// Function [`range_sort`](range_sort()).
#[pyfunction]
#[must_use]
pub fn range_sort(s: &Signal) -> SortedRange {
    assert!(!s.segments.is_empty(), "The signal must be non-empty");
    let h = s.segments[0].dimension();
    let mut l = vec![s.segments.clone()];
    let mut i = 0;
    while i < h {
        let mut new_li = Vec::with_capacity(2 * l.capacity());
        for current_l in l {
            let (pre_im0, pre_im1): (Vec<_>, Vec<_>) = current_l
                .into_iter()
                .partition(|s| s.values().value_at_pos(i) == 0);
            if !pre_im0.is_empty() {
                new_li.push(pre_im0);
            }
            if !pre_im1.is_empty() {
                new_li.push(pre_im1);
            }
        }

        i += 1;
        l = new_li;
    }
    SortedRange::from(l)
}

#[pyclass]
#[derive(Clone, Debug)]
/// Union of Ranges
pub struct RangeUnion(Vec<Range<f32>>);

impl From<SortedRange> for Vec<RangeUnion> {
    fn from(val: SortedRange) -> Self {
        assert!(!val.0.is_empty());
        val.0.into_iter().map(|item| item.1).collect()
    }
}

impl From<&[Range<f32>]> for RangeUnion {
    fn from(value: &[Range<f32>]) -> Self {
        Self(value.to_vec())
    }
}

impl RangeUnion {
    /// Returns a reference to the ranges of this [`RangeUnion`].
    #[must_use]
    pub fn ranges(&self) -> &[Range<f32>] {
        &self.0
    }

    /// Returns a mutable reference to the ranges of this [`RangeUnion`].
    #[must_use]
    pub fn ranges_mut(&mut self) -> &mut Vec<Range<f32>> {
        &mut self.0
    }

    #[must_use]
    /// Create a new empty [`RangeUnion`].
    pub const fn empty() -> Self {
        Self(Vec::new())
    }
}

#[pymethods]
impl RangeUnion {
    /// Creates a new [`RangeUnion`].
    #[new]
    fn new(values: Vec<[f32; 2]>) -> Self {
        let vals = values.iter().map(|x| (x[0])..(x[1])).collect();
        Self(vals)
    }
}

impl Add for RangeUnion {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        let self_vec = self.0;
        let rhs_vec = rhs.0;
        let mut merged_vec = Vec::with_capacity(self_vec.len() + rhs_vec.len());
        let mut iter_self = self_vec.into_iter();
        let mut iter_rhs = rhs_vec.into_iter();

        let mut next_self = iter_self.next();
        let mut next_rhs = iter_rhs.next();

        while let (Some(range_self), Some(range_rhs)) = (&next_self, &next_rhs)
        {
            if range_self.start < range_rhs.start
                || (range_self.start == range_rhs.start
                    && range_self.end <= range_rhs.end)
            {
                merged_vec.push(next_self.take().unwrap());
                next_self = iter_self.next();
            } else {
                merged_vec.push(next_rhs.take().unwrap());
                next_rhs = iter_rhs.next();
            }
        }
        merged_vec.extend(next_self);
        merged_vec.extend(iter_self);
        merged_vec.extend(next_rhs);
        merged_vec.extend(iter_rhs);

        Self(merged_vec)
    }
}

#[pyclass]
#[derive(Clone, Debug)]
/// Element of a [`SortedRange`].
pub struct SortedRangeItem(IntValues, RangeUnion);

impl From<(IntValues, RangeUnion)> for SortedRangeItem {
    fn from(value: (IntValues, RangeUnion)) -> Self {
        Self(value.0, value.1)
    }
}

#[pymethods]
impl SortedRangeItem {
    #[must_use]
    /// Function for displaying in a Python notebook.
    pub fn __repr__(&self) -> String {
        let ranges = self
            .1
             .0
            .iter()
            .map(|r| format!("[{}, {}]", r.start, r.end))
            .collect::<Vec<_>>()
            .join(" ");
        let vec: Vec<_> = self.0.bits_it().collect();
        format!("{vec:?}: {ranges}")
    }

    #[must_use]
    #[pyo3(name = "values")]
    /// Python binding of [`values`](Self::values).
    pub const fn values_py(&self) -> IntValues {
        self.0
    }

    /// Python binding of [`ranges`](Self::ranges).
    #[must_use]
    #[pyo3(name = "ranges")]
    pub fn ranges_py(&self) -> RangeUnion {
        self.1.clone()
    }
}

impl SortedRangeItem {
    /// Returns a reference to the values of this [`SortedRangeItem`].
    #[must_use]
    pub const fn values(&self) -> &IntValues {
        &self.0
    }

    /// Returns a reference to the ranges of this [`SortedRangeItem`].
    #[must_use]
    pub const fn ranges(&self) -> &RangeUnion {
        &self.1
    }
}

#[pyclass]
#[derive(Debug)]
/// Structure describing the result of [`range_sort()`].
pub struct SortedRange(Vec<SortedRangeItem>);

impl SortedRange {
    /// Returns a reference to the items of this [`SortedRange`].
    #[must_use]
    pub fn items(&self) -> &[SortedRangeItem] {
        &self.0
    }

    /// Tests if two [`SortedRange`] are equal.
    #[must_use]
    pub fn same(left: &Self, right: &Self) -> bool {
        let left_vals = &left.0;
        let right_vals = &right.0;
        left_vals.len() == right_vals.len()
            && left_vals
                .iter()
                .map(|x| &x.0)
                .all(|x| right_vals.iter().any(|y| y.0 == *x))
    }
}

#[pymethods]
impl SortedRange {
    #[must_use]
    /// Function for displaying in a Python notebook.
    pub fn __repr__(&self) -> String {
        self.0
            .iter()
            .map(SortedRangeItem::__repr__)
            .reduce(|x, y| format!("{x}\n{y}"))
            .unwrap_or_default()
    }

    #[must_use]
    /// Method for making the structure indexable.
    fn __getitem__(&self, idx: usize) -> SortedRangeItem {
        self.0[idx].clone()
    }
}

impl From<Vec<Vec<Segment>>> for SortedRange {
    fn from(value: Vec<Vec<Segment>>) -> Self {
        let res = value
            .into_iter()
            .map(|segs| {
                let v = segs.first().unwrap().values().to_owned();
                let ranges =
                    segs.into_iter().map(|x| x.limits().clone()).collect();
                let union = RangeUnion(ranges);
                (v, union).into()
            })
            .collect();
        Self(res)
    }
}
