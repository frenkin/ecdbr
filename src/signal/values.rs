//! Module for Boolean variable valuations storage

use core::fmt::{Formatter, Result};
use std::{fmt::Debug, ops::BitAnd};

use pyo3::{pyclass, pymethods};

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[pyclass]
/// Structure containing the valuation of Boolean variables.
pub struct IntValues(usize);

impl From<Vec<u8>> for IntValues {
    fn from(value: Vec<u8>) -> Self {
        debug_assert!(
            value.iter().all(|x| *x == 0 || *x == 1),
            "Invalid value"
        );
        let size = value.len();
        let mut values = 0;
        for (i, &bit) in value.iter().enumerate() {
            if bit != 0 {
                values |= 1 << i;
            }
        }
        Self::new(size, values)
    }
}

impl BitAnd for IntValues {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self::Output {
        let size = self.len().min(rhs.len());
        let values = (self.0 & Self::VALUE_MASK) & (rhs.0 & Self::VALUE_MASK);
        Self::new(size, values)
    }
}

impl PartialEq<usize> for IntValues {
    fn eq(&self, other: &usize) -> bool {
        self.0 == *other
    }
}

impl IntValues {
    /// Number of bits used to store the number of values.
    const SIZE_BITS: usize = (usize::BITS / 8) as usize;
    /// Mask for accessing stored values.
    const VALUE_MASK: usize =
        (1 << ((usize::BITS as usize) - Self::SIZE_BITS)) - 1;

    /// Creates a new [`IntValues`].
    #[must_use]
    pub const fn new(dim: usize, val: usize) -> Self {
        Self(
            (dim << ((usize::BITS as usize) - Self::SIZE_BITS))
                | (val & Self::VALUE_MASK),
        )
    }

    /// Returns the values of this [`IntValues`] as a [`usize`].
    #[must_use]
    pub const fn values(&self) -> usize {
        self.0 & Self::VALUE_MASK
    }

    /// Returns the value at position `pos`.
    #[must_use]
    pub const fn value_at_pos(&self, pos: usize) -> u8 {
        if self.0 & (1 << pos) != 0 {
            1
        } else {
            0
        }
    }

    /// Returns the bits it of this [`IntValues`] as an Iterator.
    pub fn bits_it(&self) -> impl Iterator<Item = u8> {
        BitIterator::new(*self)
    }

    /// Returns the number of values of this [`IntValues`].
    #[must_use]
    pub const fn len(&self) -> usize {
        self.0 >> (usize::BITS as usize - Self::SIZE_BITS)
    }

    /// Checks if this [`IntValues`] is empty .
    #[must_use]
    pub const fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

#[pymethods]
impl IntValues {
    #[new]
    /// Python constructor.
    fn new_py(vals: Vec<u8>) -> Self {
        vals.into()
    }

    /// Returns the values as a vector of bits stored as u8.
    #[must_use]
    pub fn bits(&self) -> Vec<u8> {
        self.bits_it().collect()
    }
}

impl Debug for IntValues {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        let res = (0..self.len())
            .map(|i| self.value_at_pos(i).to_string())
            .reduce(|x, y| format!("{x}, {y}"))
            .map_or_else(|| "[]".to_owned(), |s| format!("[{s}]"));

        f.write_str(res.as_str())
    }
}

/// Structure for iterating over bits .
struct BitIterator {
    /// Bits.
    values: usize,
    /// Current position.
    position: usize,
    /// Number of values
    size: usize,
}

impl BitIterator {
    /// Creates a new [`BitIterator`].
    #[must_use]
    const fn new(value: IntValues) -> Self {
        let size = value.len();
        Self {
            values: value.0,
            position: 0,
            size,
        }
    }
}

impl Iterator for BitIterator {
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item> {
        (self.position < self.size).then(|| {
            let bit = u8::from((self.values & (1 << self.position)) != 0);
            self.position += 1;
            bit
        })
    }
}
