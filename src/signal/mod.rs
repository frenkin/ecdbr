use core::{f32, ops::Range};
use pyo3::types::{PyAnyMethods, PyModule};
use pyo3::{pyclass, pymethods, Py, Python};
use segment::Segment;
use values::IntValues;

use crate::plot::{self, Plot};
use crate::stl::algos::verify::range_intersect;

pub mod algorithms;
pub mod segment;
pub mod values;

/// Boolean signal.
#[pyclass]
#[derive(Clone, Debug)]
pub struct Signal {
    /// Set of [`Segment`]s.
    segments: Vec<Segment>,
}

impl From<Vec<(&Range<f32>, IntValues)>> for Signal {
    fn from(value: Vec<(&Range<f32>, IntValues)>) -> Self {
        assert!(
            value.is_sorted_by(|x, y| x.0.start < y.0.start),
            "Invalid signal"
        );
        let segments: Vec<Segment> = value
            .iter()
            .map(|(r, v)| Segment::new(r.start, *v, r.end))
            .collect();

        Self::new(&segments)
    }
}

impl Signal {
    /// Returns the segment containing `pos` in its range.
    #[must_use]
    pub(crate) fn segment_at(&self, pos: f32) -> &Segment {
        self.segments
            .iter()
            .rev()
            .find(|s| range_intersect(s.limits(), &(pos..pos)))
            .unwrap_or_else(|| panic!("{pos} is not a valid moment"))
    }

    /// Returns the segment containing lim_{t' → t} ω(t)
    #[must_use]
    pub(crate) fn segment_before(&self, pos: f32) -> &Segment {
        self.segments
            .iter()
            .find(|s| range_intersect(s.limits(), &(pos..pos)))
            .unwrap_or_else(|| panic!("{pos} is not a valid moment"))
    }

    /// Returns a reference to the segments of this [`Signal`].
    pub(crate) fn segments(&self) -> &[Segment] {
        &self.segments
    }

    /// Creates a new [`Signal`].
    #[must_use]
    pub fn new(segments: &[Segment]) -> Self {
        let mut result = Vec::new();
        let mut iter = segments.iter().peekable();

        while let Some(seg1) = iter.next() {
            let mut last_high = seg1.high();
            while let Some(&seg_last) = iter.peek() {
                if *seg1.values() == *seg_last.values() {
                    last_high = seg_last.high();
                    iter.next();
                } else {
                    break;
                }
            }
            result.push(Segment::new(seg1.low(), *seg1.values(), last_high));
        }

        Self { segments: result }
    }
}

#[pymethods]
impl Signal {
    /// Function for displaying in a Python notebook.
    fn __repr__(s: Py<Self>) -> String {
        Python::with_gil(|py| {
            let functions = PyModule::from_code_bound(
                py,
                include_str!("../aux.py"),
                "aux.py",
                "aux",
            )
            .unwrap();
            let fu = functions.getattr("display_signal").unwrap();
            let _ = fu.call1((s,));
        });
        String::default()
    }

    /// Creates a new [`Signal`].
    ///
    /// # Panics
    ///
    /// Panics if `segments` is empty.
    #[new]
    fn new_py(segments: Vec<Segment>) -> Self {
        assert!(!segments.is_empty(), "A signal can not be empty");
        Self::new(&segments)
    }

    /// Python binding of [`segments`](Self::segments()).
    #[pyo3(name = "segments")]
    fn segments_py(&self) -> Vec<Segment> {
        self.segments.clone()
    }

    /// Returns the dimension of the [`Signal`].
    #[must_use]
    pub fn dimension(&self) -> usize {
        self.segments[0].dimension()
    }

    /// Python binding of `segment_at`.
    #[pyo3(name = "segment_at")]
    #[must_use]
    pub fn segment_at_py(&self, pos: f32) -> Segment {
        self.segment_at(pos).clone()
    }

    /// Convert the [`Signal`] into a [`Plot`].
    #[must_use]
    pub fn to_plot(&self, var: usize) -> Plot {
        let segments: Vec<_> = self
            .segments
            .iter()
            .map(|s| {
                plot::Segment::new(
                    s.limits().clone(),
                    0.,
                    f32::from(s.values().value_at_pos(var)),
                )
            })
            .collect();
        Plot::new(segments.as_slice())
    }

    #[must_use]
    /// Maximum time of a Signal.
    pub fn last_moment(&self) -> Option<f32> {
        self.segments.last().map(Segment::high)
    }
}
