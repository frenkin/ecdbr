pub mod plot;
pub mod signal;
pub mod stl;

use plot::operation::Operation;
use pyo3::{prelude::*, types::PyModule};
use signal::{
    algorithms::{
        dist::dist,
        h_dist::h_dist,
        range_sort::{range_sort, RangeUnion, SortedRangeItem},
    },
    segment::Segment,
    values::IntValues,
    Signal,
};

use stl::{
    algos::{
        python::{
            algo10_py, algo11_py, algo12_py, algo13_py, algo14_py, algo15_py,
            algo16_py, algo17_py, algo18_py, algo4_py, algo8_py, algo9_py,
            colour_py, d_py,
        },
        verify::verify_py,
    },
    stl::formula::Formula,
};

#[pymodule]
fn ecdbr(m: &Bound<'_, PyModule>) -> PyResult<()> {
    m.add_class::<Signal>()?;
    m.add_class::<Segment>()?;
    m.add_class::<SortedRangeItem>()?;
    m.add_class::<RangeUnion>()?;
    m.add_class::<IntValues>()?;
    m.add_class::<Formula>()?;
    m.add_class::<Operation>()?;
    m.add_function(wrap_pyfunction!(range_sort, m)?)?;
    m.add_function(wrap_pyfunction!(h_dist, m)?)?;
    m.add_function(wrap_pyfunction!(dist, m)?)?;
    m.add_function(wrap_pyfunction!(algo4_py, m)?)?;
    m.add_function(wrap_pyfunction!(algo8_py, m)?)?;
    m.add_function(wrap_pyfunction!(algo9_py, m)?)?;
    m.add_function(wrap_pyfunction!(algo10_py, m)?)?;
    m.add_function(wrap_pyfunction!(algo11_py, m)?)?;
    m.add_function(wrap_pyfunction!(algo12_py, m)?)?;
    m.add_function(wrap_pyfunction!(algo13_py, m)?)?;
    m.add_function(wrap_pyfunction!(algo14_py, m)?)?;
    m.add_function(wrap_pyfunction!(algo15_py, m)?)?;
    m.add_function(wrap_pyfunction!(algo16_py, m)?)?;
    m.add_function(wrap_pyfunction!(algo17_py, m)?)?;
    m.add_function(wrap_pyfunction!(algo18_py, m)?)?;
    m.add_function(wrap_pyfunction!(colour_py, m)?)?;
    m.add_function(wrap_pyfunction!(d_py, m)?)?;
    m.add_function(wrap_pyfunction!(verify_py, m)?)?;
    Ok(())
}
