use crate::{
    plot::{operation::Operation, Plot},
    signal::Signal,
    stl::stl::B,
};

use super::val_of;

/// Algorithm for computing t ↦ d(ω, φ) where φ = F_\[t+a, b\](p)
#[must_use]
pub fn algo15(w: &Signal, p: &B, a: f32, b: f32) -> Plot {
    let wp = w.to_plot(p.var());
    let np = val_of(&p.negate());
    let p = val_of(p);
    let w_m = Operation::TTranslate(-(a + b))
        .apply(&[&Operation::TDilate(2.).apply(&[&wp])]);

    let m_prev = Operation::Prev(np).apply(&[&w_m]);
    let m_prev = Operation::STranslate(a + b).apply(&[&m_prev]);
    let m_prev = Operation::SDilate(2.).apply(&[&m_prev]);
    let lm_plot =
        Operation::STranslate(-a)
            .apply(&[&Operation::Anti()
                .apply(&[&Operation::IdDiff().apply(&[&m_prev])])]);

    let m_next = Operation::Next(np).apply(&[&w_m]);
    let m_next = Operation::STranslate(a + b).apply(&[&m_next]);
    let m_next = Operation::SDilate(2.).apply(&[&m_next]);
    let rm_plot =
        Operation::STranslate(b).apply(&[&Operation::Anti().apply(&[&m_next])]);

    let w_a = Operation::TTranslate(-a).apply(&[&wp]);
    let a_next = Operation::Next(p).apply(&[&w_a]);
    let a_curr =
        Operation::SDilate(2.).apply(&[&Operation::CurrSeg(np).apply(&[&w_a])]);
    let lc_plot = Operation::Min().apply(&[
        &Operation::Anti().apply(&[&Operation::IdDiff().apply(&[&a_next])]),
        &a_curr,
    ]);

    let rc_plot = Operation::Const(f32::min(
        b - Operation::Prev(p)
            .apply(&[&wp])
            .value_at(b)
            .unwrap_or_else(|| panic!("{b} is not a valid moment")),
        Operation::CurrSeg(np)
            .apply(&[&wp])
            .value_at(b)
            .unwrap_or_else(|| panic!("{b} is not a valid moment"))
            / 2.,
    ))
    .apply(&[]);

    let m1 = Operation::Max().apply(&[&lm_plot, &rm_plot]);
    let m2 = Operation::Max().apply(&[&lc_plot, &rc_plot]);

    Operation::Max().apply(&[&m1, &m2])
}
