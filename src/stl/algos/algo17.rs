use crate::{
    plot::operation::Operation,
    signal::{segment::Segment, values::IntValues, Signal},
    stl::{
        algos::{algo14::algo14, algo9::algo9, algo10::algo10, algo11::algo11},
        stl::B,
    },
};

use super::algo6::val_of16;

/// Creates a copy of `w` where a [`Segment`] with value `val` by a [`Segment`]
/// with value 1. Otherwise the value is 0.
fn replace(w: &Signal, val: IntValues) -> Signal {
    let segments: Vec<Segment> = w
        .segments()
        .iter()
        .map(|s| {
            let code = usize::from(*s.values() == val);
            Segment::new(s.low(), IntValues::new(1, code), s.high())
        })
        .collect();
    Signal::new(&segments)
}

#[must_use]
/// Algorithm for computing d(ω, φ) where φ = ¬(p U_[a, b] q).
pub fn algo17(w: &Signal, p: &B, a: f32, b: f32, q: &B) -> f32 {
    let np_val = val_of16(&p.negate());
    let nq_val = val_of16(&q.negate());
    let ns = replace(w, IntValues::new(2, np_val | nq_val));
    let pq_plot = algo11(&ns, &B::P(0));

    let nq = q.negate();
    let g_plot = algo14(w, a, 0., &nq);

    let d_plot = Operation::Max().apply(&[&pq_plot, &g_plot]);

    let np = p.negate();
    let values = [
        algo10(w, a, b, &nq),
        algo9(w, 0., a, &np),
        d_plot.min(a, b).unwrap(),
    ];
    *values
        .iter()
        .min_by(|arg0: _, other: _| f32::total_cmp(arg0, other))
        .unwrap_or(&0.)
}
