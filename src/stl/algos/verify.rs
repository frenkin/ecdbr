use std::ops::Range;

use pyo3::pyfunction;

use crate::{
    signal::{segment::Segment, Signal},
    stl::stl::{formula::Formula, StlR, B},
};

/// Checks if two intervals intersect.
#[must_use]
pub fn range_intersect(r1: &Range<f32>, r2: &Range<f32>) -> bool {
    r1.start <= r2.end && r2.start <= r1.end
}

/// Checks if a [`Segment`] verifies a [`StlR`] formula.
#[must_use]
pub fn verify_seg(segment: &Segment, formula: &B) -> bool {
    match formula {
        B::P(pos) => segment.values().bits()[*pos] == 1,
        B::Disj(l, r) => verify_seg(segment, l) || verify_seg(segment, r),
        B::Neg(b) => !verify_seg(segment, b),
    }
}

#[pyfunction]
#[pyo3(name = "verify")]
#[must_use]
/// Python binding of [`verify`].
pub fn verify_py(w: &Signal, f: &Formula) -> bool {
    verify(w, &f.stl_r)
}

/// Checks if a [`Signal`] satisfies a [`StlR`] formula.
#[must_use]
pub fn verify(w: &Signal, formula: &StlR) -> bool {
    match formula {
        StlR::B(b) => verify_seg(&w.segments()[0], b),
        StlR::G(range, b) => w
            .segments()
            .iter()
            .filter(|s| range_intersect(s.limits(), range))
            .all(|s| verify_seg(s, b)),
        StlR::U(range, b1, b2) => {
            match w
                .segments()
                .iter()
                .enumerate()
                .filter(|(_, s)| range_intersect(s.limits(), range))
                .find(|(_, x)| verify_seg(x, b2))
            {
                Some((pos, _)) => {
                    w.segments()[0..=pos].iter().all(|seg| verify_seg(seg, b1))
                },
                None => false,
            }
        },
        StlR::GFI(b1, b, b2) => {
            let valid_ranges = w
                .segments()
                .iter()
                .filter(|seg| verify_seg(seg, b2))
                .map(|seg| (seg.low() - b)..seg.high())
                .fold(Vec::new(), |mut merged: Vec<Range<_>>, curr| {
                    if let Some(r) = merged.last_mut() {
                        if r.end >= curr.start {
                            r.end = curr.end;
                        } else {
                            merged.push(curr);
                        }
                    } else {
                        merged.push(curr);
                    }
                    merged
                });

            w.segments()
                .iter()
                .filter(|seg| verify_seg(seg, b1))
                .all(|seg| {
                    valid_ranges
                        .iter()
                        .any(|r| r.contains(&seg.low()) && seg.high() <= r.end)
                })
        },
        StlR::Neg(stl_r) => !verify(w, stl_r),
        StlR::Disj(stl_r, stl_r1) => verify(w, stl_r) || verify(w, stl_r1),
    }
}
