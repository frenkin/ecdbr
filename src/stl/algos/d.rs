use crate::{
    signal::Signal,
    stl::{
        algos::{algo8::algo8, algo9::algo9},
        stl::StlR,
    },
};

use super::{
    algo16::algo16, algo17::algo17, algo18::algo18, algo6::algo6,
    algo10::algo10,
};

/// Function for calculating the distance between a [`Signal`] and an
/// [`STLr`](StlR) formula.
#[must_use]
pub fn d(w: &Signal, phi: StlR) -> f32 {
    match phi {
        StlR::B(b) => algo8(w, &b, 0.),
        StlR::G(range, b) => algo10(w, range.start, range.end, &b),
        StlR::U(range, b, b1) => algo16(w, &b, &b1, range.start, range.end),
        StlR::GFI(p, b, q) => algo6(w, &p, b, &q),
        StlR::Neg(stl_r) => match *stl_r {
            StlR::U(range, b, b1) => algo17(w, &b, range.start, range.end, &b1),
            StlR::GFI(b1, b, b2) => algo18(w, &b1, &b2, b),
            StlR::G(range, b) => {
                let nb = b.negate();
                algo9(w, range.start, range.end, &nb)
            },
            StlR::Neg(_) | StlR::Disj(..) | StlR::B(_) => unreachable!(),
        },
        StlR::Disj(..) => unreachable!(),
    }
}
