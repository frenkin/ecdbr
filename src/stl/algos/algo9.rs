use crate::{plot::operation::Operation, signal::Signal, stl::stl::B};

use super::val_of;

#[must_use]
/// Algorithm for computing d(ω, φ) where φ = F_\[a, b\](p)
pub fn algo9(w: &Signal, a: f32, b: f32, p: &B) -> f32 {
    let wp = w.to_plot(p.var());
    let p = val_of(p);
    let l = Operation::IdDiff()
        .apply(&[&Operation::Prev(p).apply(&[&wp])])
        .value_at(a)
        .unwrap_or(f32::INFINITY);
    let r = Operation::Anti()
        .apply(&[
            &Operation::IdDiff().apply(&[&Operation::Next(p).apply(&[&wp])])
        ])
        .value_at(b)
        .unwrap_or(f32::INFINITY);
    f32::min(l, r)
}
