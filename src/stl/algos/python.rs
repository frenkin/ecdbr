use pyo3::pyfunction;

use crate::{
    plot::Plot,
    signal::Signal,
    stl::stl::{formula::Formula, StlR},
};

use super::{
    algo10::algo10, algo11::algo11, algo12::algo12, algo13::algo13,
    algo14::algo14, algo15::algo15, algo16::algo16, algo17::algo17,
    algo18::algo18, algo4::algo4, algo6::algo6, algo8::algo8, algo9::algo9,
    colour::colour, d::d,
};

#[pyfunction]
#[pyo3(name = "algo4")]
#[must_use]
/// Python binding of [`algo4`].
pub fn algo4_py(w: &Signal, p: &Formula) -> f32 {
    algo4(w, p.stl_r.clone())
}

#[pyfunction]
#[pyo3(name = "algo8")]
#[must_use]
/// Python binding of [`algo8`].
pub fn algo8_py(w: &Signal, p: &Formula, a: f32) -> f32 {
    match &p.stl_r {
        StlR::B(s) => algo8(w, s, a),
        _ => unimplemented!("p should be a variable"),
    }
}

#[pyfunction]
#[pyo3(name = "algo9")]
#[must_use]
/// Python binding of [`algo9`].
pub fn algo9_py(w: &Signal, p: &Formula, a: f32, b: f32) -> f32 {
    match &p.stl_r {
        StlR::B(s) => algo9(w, a, b, s),
        _ => unimplemented!("p should be a variable"),
    }
}

#[pyfunction]
#[pyo3(name = "algo10")]
#[must_use]
/// Python binding of [`algo10`].
pub fn algo10_py(w: &Signal, a: f32, b: f32, p: &Formula) -> f32 {
    match &p.stl_r {
        StlR::B(s) => algo10(w, a, b, s),
        _ => unimplemented!("p should be a variable"),
    }
}

#[pyfunction]
#[pyo3(name = "algo11")]
#[must_use]
/// Python binding of [`algo11`].
pub fn algo11_py(w: &Signal, p: &Formula) -> Plot {
    match &p.stl_r {
        StlR::B(s) => algo11(w, s),
        _ => unimplemented!("p should be a variable"),
    }
}

#[pyfunction]
#[pyo3(name = "algo12")]
#[must_use]
/// Python binding of [`algo12`].
pub fn algo12_py(w: &Signal, a: f32, b: f32, p: &Formula) -> Plot {
    match &p.stl_r {
        StlR::B(s) => algo12(w, s, a, b),
        _ => unimplemented!("p should be a variable"),
    }
}

#[pyfunction]
#[pyo3(name = "algo13")]
#[must_use]
/// Python binding of [`algo13`].
pub fn algo13_py(w: &Signal, a: f32, b: f32, p: &Formula) -> Plot {
    match &p.stl_r {
        StlR::B(s) => algo13(w, a, b, s),
        _ => unimplemented!("p should be a variable"),
    }
}

#[pyfunction]
#[pyo3(name = "algo14")]
#[must_use]
/// Python binding of [`algo14`].
pub fn algo14_py(w: &Signal, a: f32, b: f32, p: &Formula) -> Plot {
    match &p.stl_r {
        StlR::B(s) => algo14(w, a, b, s),
        _ => unimplemented!("p should be a variable"),
    }
}

#[pyfunction]
#[pyo3(name = "algo15")]
#[must_use]
/// Python binding of [`algo15`].
pub fn algo15_py(w: &Signal, a: f32, b: f32, p: &Formula) -> Plot {
    match &p.stl_r {
        StlR::B(s) => algo15(w, s, a, b),
        _ => unimplemented!("p should be a variable"),
    }
}

#[pyfunction]
#[pyo3(name = "algo16")]
#[must_use]
/// Python binding of [`algo16`].
pub fn algo16_py(w: &Signal, a: f32, b: f32, p: &Formula, q: &Formula) -> f32 {
    match (&p.stl_r, &q.stl_r) {
        (StlR::B(s1), StlR::B(s2)) => algo16(w, s1, s2, a, b),
        _ => unimplemented!("p and q should be variables"),
    }
}

#[pyfunction]
#[pyo3(name = "algo17")]
#[must_use]
/// Python binding of [`algo17`].
pub fn algo17_py(w: &Signal, a: f32, b: f32, p: &Formula, q: &Formula) -> f32 {
    match (&p.stl_r, &q.stl_r) {
        (StlR::B(s1), StlR::B(s2)) => algo17(w, s1, a, b, s2),
        _ => unimplemented!("p and q should be variables"),
    }
}

#[pyfunction]
#[pyo3(name = "algo18")]
#[must_use]
/// Python binding of [`algo18`].
pub fn algo18_py(w: &Signal, b: f32, p: &Formula, q: &Formula) -> f32 {
    match (&p.stl_r, &q.stl_r) {
        (StlR::B(s1), StlR::B(s2)) => algo18(w, s1, s2, b),
        _ => unimplemented!("p and q should be variables"),
    }
}

#[pyfunction]
#[pyo3(name = "algo6")]
#[must_use]
/// Python binding of [`algo6`].
pub fn algo6_py(w: &Signal, b: f32, p: &Formula, q: &Formula) -> f32 {
    match (&p.stl_r, &q.stl_r) {
        (StlR::B(s1), StlR::B(s2)) => algo6(w, s1, b, s2),
        _ => unimplemented!("p and q should be variables"),
    }
}

/// Python binding of [`colour`].
#[pyfunction]
#[pyo3(name = "colour", signature = (s, p, f1, f2=None))]
#[must_use]
pub fn colour_py(
    s: &Signal,
    p: &Formula,
    f1: &Formula,
    f2: Option<&Formula>,
) -> (Signal, Formula) {
    let bs: Vec<_> = f2
        .map_or_else(|| vec![f1], |f| vec![f1, f])
        .iter()
        .map(|f| match &f.stl_r {
            StlR::B(x) => &**x,
            StlR::G(..)
            | StlR::U(..)
            | StlR::GFI(..)
            | StlR::Neg(_)
            | StlR::Disj(..) => unreachable!(),
        })
        .collect();
    let (s, f) = colour(s, &p.stl_r, bs.as_slice());
    (s, Formula { stl_r: f })
}

#[pyfunction]
#[pyo3(name = "d")]
#[must_use]
/// Python binding of [`d`].
pub fn d_py(w: &Signal, phi: &Formula) -> f32 {
    d(w, phi.stl_r.clone())
}
