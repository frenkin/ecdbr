use crate::{plot::operation::Operation, signal::Signal, stl::stl::B};

use super::val_of;

#[must_use]
/// Algorithm for computing d(ω, φ) where φ = p(a)
pub fn algo8(w: &Signal, phi: &B, a: f32) -> f32 {
    let w_plot = w.to_plot(phi.var());
    let p = val_of(phi);

    let pr = Operation::Prev(p);
    let pr_plot = pr.apply(&[&w_plot]);

    let l = Operation::IdDiff()
        .apply(&[&pr_plot])
        .value_at(a)
        .unwrap_or(f32::INFINITY);

    let ne_plot = Operation::Next(p).apply(&[&w_plot]);
    let id_ne = Operation::IdDiff().apply(&[&ne_plot]);
    let r = Operation::Anti()
        .apply(&[&id_ne])
        .value_at(a)
        .unwrap_or(f32::INFINITY);
    f32::min(l, r)
}
