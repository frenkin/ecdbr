use crate::{
    signal::Signal,
    stl::{algos::colour, stl::StlR},
};

use super::{d::d, verify::verify};

/// Algorithm for computing δ(ω, φ).
#[must_use]
pub fn algo4(w: &Signal, phi: StlR) -> f32 {
    let mut sign = -1.;
    let (w, mut p) = match phi {
        StlR::Disj(l, r) => return f32::max(algo4(w, *l), algo4(w, *r)),
        StlR::Neg(p2) => return -1. * algo4(w, *p2),
        StlR::B(ref b) | StlR::G(_, ref b) => colour::colour(w, &phi, &[b]),
        StlR::U(_, ref p1, ref p2) | StlR::GFI(ref p1, _, ref p2) => {
            colour::colour(w, &phi, &[p1, p2])
        },
    };
    if verify(&w, &p) {
        sign = 1.;
        p = StlR::not(p);
    }
    sign * d(&w, p)
}
