use crate::{
    plot::{operation::Operation, Plot},
    signal::Signal,
    stl::stl::B,
};

use super::val_of;

#[must_use]
/// Algorithm for computing t ↦ d(ω, φ) where φ = F_\[t+a, t+b\](p)
pub fn algo12(w: &Signal, p: &B, a: f32, b: f32) -> Plot {
    let wp = w.to_plot(p.var());
    let p = val_of(p);
    let w_a = Operation::TTranslate(-a).apply(&[&wp]);
    let l_plot =
        Operation::IdDiff().apply(&[&Operation::Prev(p).apply(&[&w_a])]);

    let w_b = Operation::TTranslate(-b).apply(&[&wp]);
    let r_plot =
        Operation::Anti()
            .apply(&[&Operation::IdDiff()
                .apply(&[&Operation::Next(p).apply(&[&w_b])])]);
    Operation::Min().apply(&[&l_plot, &r_plot])
}
