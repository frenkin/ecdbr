use crate::{
    plot::operation::Operation,
    signal::Signal,
    stl::{algos::algo11::algo11, stl::B},
};

use super::algo14::algo14;

#[must_use]
/// Algorithm for computing d(ω, φ) where φ = p U_\[a,b\] q.
pub fn algo16(w: &Signal, p: &B, q: &B, a: f32, b: f32) -> f32 {
    let q_plot = algo11(w, q);
    let g_plot = algo14(w, 0., 0., p);
    let d_plot = Operation::Max().apply(&[&q_plot, &g_plot]);
    d_plot.min(a, b).unwrap()
}
