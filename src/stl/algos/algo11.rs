use crate::{
    plot::{operation::Operation, Plot},
    signal::Signal,
    stl::stl::B,
};

use super::val_of;

#[must_use]
/// Algorithm for computing t ↦ d(ω, φ) where φ = p(t)
pub fn algo11(w: &Signal, p: &B) -> Plot {
    let wp = w.to_plot(p.var());
    let p = val_of(p);
    let l_plot_prev = Operation::Prev(p).apply(&[&wp]);
    let l_plot = Operation::IdDiff().apply(&[&l_plot_prev]);

    let r_plot_next = Operation::Next(p).apply(&[&wp]);
    let r_id_next = Operation::IdDiff().apply(&[&r_plot_next]);
    let r_plot = Operation::Anti().apply(&[&r_id_next]);

    Operation::Min().apply(&[&l_plot, &r_plot])
}
