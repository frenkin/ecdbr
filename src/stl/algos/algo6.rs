use crate::{
    signal::{segment::Segment, Signal},
    stl::{
        algos::verify::{range_intersect, verify_seg},
        stl::{StlR, B},
    },
};

/// Converts a B into a usize.
#[must_use]
pub fn val_of16(p: &B) -> usize {
    match p {
        B::Neg(x) => {
            assert!(!matches!(**x, B::Neg(_)));
            0
        },
        B::P(_) => 1,
        _ => unreachable!(),
    }
}

use super::algo4::algo4;

/// Line 17 of the algorithm.
fn l17(w: &Signal, u: f32, v: f32, p: &B) -> f32 {
    let first_segment = w
        .segments()
        .iter()
        .find(|s| range_intersect(&(u..v), s.limits()) && verify_seg(s, p))
        .unwrap();
    v - f32::max(first_segment.low(), u)
}

/// Line 18 of the algorithm.
fn l18(w: &Signal, u: f32, v: f32, p: &B, q: &B) -> bool {
    let np_val = val_of16(&p.negate());
    let nq_val = val_of16(&q.negate());
    let code = np_val | nq_val;
    w.segments().iter().any(|s| {
        range_intersect(&(u..v), s.limits()) && s.values().values() == code
    })
}

/// Line 19 of the algorithm.
fn l19(w: &Signal, v: f32, l: f32, b: f32, p: &B, u: f32) -> f32 {
    let low = u + v - l;
    let high = u + low + (l - b) / 2.;
    let np = p.negate();
    let formula = StlR::G(low..high, Box::new(np));
    let dist = algo4(w, formula);
    -1. * dist
}

/// Line 25 of the algorithm.
fn l25(w: &Signal, u: f32, v: f32, l: f32, b: f32, p: &B, q: &B) -> f32 {
    let p_val = val_of16(p);
    let nq_val = val_of16(&q.negate());
    let code = p_val | nq_val;
    let pos = u + (l - b) / 2.;
    w.segments()
        .iter()
        .rev()
        .find(|s| {
            s.low() <= pos
                && range_intersect(&(u..v), s.limits())
                && s.values().values() == code
        })
        .map_or(f32::NEG_INFINITY, |s| f32::min(pos, s.high()))
}

/// Line 26 of the algorithm.
fn l26(w: &Signal, u: f32, v: f32, l: f32, b: f32, p: &B, q: &B) -> f32 {
    let p_val = val_of16(p);
    let nq_val = val_of16(&q.negate());
    let code = p_val | nq_val;
    let pos = u + (l - b) / 2.;
    w.segments()
        .iter()
        .find(|s| {
            s.high() >= pos
                && range_intersect(&(u..v), s.limits())
                && s.values().values() == code
        })
        .map_or(f32::INFINITY, |s| f32::max(pos, s.low()))
}

/// Line 27 of the algorithm.
fn l27(w: &Signal, l: f32, b: f32, p: &B, q: &B, u: f32) -> f32 {
    assert!(w
        .segments()
        .windows(2)
        .all(|vals| vals[0].values() != vals[1].values()));
    let p_val = val_of16(p);
    let nq_val = val_of16(&q.negate());
    let pos = u + (l - b) / 2.;
    w.segments()
        .iter()
        .rev()
        .find(|x| (x.low()..=x.high()).contains(&pos))
        .map_or(0., |seg| {
            let seg_val = seg.values();
            if *seg_val == (p_val | nq_val) {
                let low = seg.low();
                let high = seg.high();
                let low_diff = pos - low;
                let high_diff = high - pos;
                f32::min(low_diff, high_diff)
            } else {
                0.
            }
        })
}

/// Line 30 of the algorithm.
fn l30(w: &Signal, u: f32, v: f32, p: &B) -> f32 {
    let last_segment_high = w
        .segments()
        .iter()
        .rev()
        .find(|s| range_intersect(&(u..v), s.limits()) && verify_seg(s, p))
        .map_or(f32::NEG_INFINITY, Segment::high);
    f32::min(last_segment_high, v) - u
}

/// Line 31 of the algorithm.
fn l31(w: &Signal, u: f32, v: f32, p: &B, q: &B) -> bool {
    let np_val = val_of16(&p.negate());
    let nq_val = val_of16(&q.negate());
    let code = np_val | nq_val;
    w.segments().iter().any(|s| {
        range_intersect(s.limits(), &(u..v)) && s.values().values() == code
    })
}

/// Line 32 of the algorithm.
fn l32(w: &Signal, v: f32, l: f32, p: &B) -> f32 {
    let low = v - l / 2.;
    let high = v;
    let np = p.negate();
    let formula = StlR::G(low..high, Box::new(np));
    let dist = algo4(w, formula);
    -1. * dist
}

/// Algorithm for computing d(ω, φ) where φ = G(p) ⇒ F_\[0, b\](q).
#[must_use]
pub fn algo6(w: &Signal, p: &B, b: f32, q: &B) -> f32 {
    let mut r = vec![];
    let segments = w.segments();
    let mut u = 0.;
    let mut v = 0.;
    let mut it = segments.iter();
    it.next();
    for seg_i in it {
        if verify_seg(seg_i, q) {
            if u < v {
                r.push((u, v));
            }
            u = seg_i.high();
        } else {
            v = seg_i.high();
        }
    }
    r.push((u, segments.last().unwrap().high()));
    if r.len() == 1 && u == 0. {
        return f32::INFINITY;
    }
    let mut d = f32::NEG_INFINITY;
    for (u, v) in r {
        if u == v {
            continue;
        }
        if u == 0. && !verify_seg(&segments[0], q) {
            let l = l17(w, u, v, p);
            if l18(w, u, v, p, q) {
                let c = l19(w, v, l, b, p, u);
                d = *[d, (l - b) / 2., c]
                    .iter()
                    .max_by(|arg0: _, other: _| f32::total_cmp(arg0, other))
                    .unwrap();
            } else {
                d = f32::max(d, l - b);
            }
        } else if verify_seg(w.segment_before(u), q)
            && verify_seg(w.segment_at(v), q)
        {
            let l = v - u;
            let m = l25(w, u, v, l, b, p, q);
            let mp = l26(w, u, v, l, b, p, q);
            let c = l27(w, l, b, p, q, u);
            d = *[2. * d, m - u, v - b - mp, c]
                .iter()
                .max_by(|arg0: _, other: _| f32::total_cmp(arg0, other))
                .unwrap_or(&0.)
                / 2.;
        } else {
            let l = l30(w, u, v, p);
            if l31(w, u, v, p, q) {
                let c = l32(w, v, l, p);
                d = *[d, c, l / 2.]
                    .iter()
                    .max_by(|arg0: _, other: _| f32::total_cmp(arg0, other))
                    .unwrap_or(&0.);
            } else {
                d = f32::max(d, l);
            }
        }
    }
    d
}
