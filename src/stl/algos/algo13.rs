use crate::{
    plot::{operation::Operation, Plot},
    signal::Signal,
    stl::stl::B,
};

use super::val_of;

#[must_use]
/// Algorithm for computing t ↦ d(ω, φ) where φ = G_\[t+a, t+b\](p)
pub fn algo13(w: &Signal, a: f32, b: f32, p: &B) -> Plot {
    let wp = w.to_plot(p.var());
    let np = val_of(&p.negate());
    let p = val_of(p);
    let w_m = Operation::TTranslate(-(a + b) / 2.).apply(&[&wp]);
    let m_prev = Operation::Prev(np).apply(&[&w_m]);
    let lm_plot =
        Operation::STranslate((b - a) / 2.)
            .apply(&[&Operation::Anti()
                .apply(&[&Operation::IdDiff().apply(&[&m_prev])])]);

    let m_next = Operation::Next(np).apply(&[&w_m]);
    let rm_plot = Operation::STranslate((b - a) / 2.)
        .apply(&[&Operation::IdDiff().apply(&[&m_next])]);

    let wa = Operation::TTranslate(-a).apply(&[&wp]);
    let a_next = Operation::Next(p).apply(&[&wa]);
    let a_curr =
        Operation::SDilate(2.).apply(&[&Operation::CurrSeg(np).apply(&[&wa])]);
    let lc_plot = Operation::Min().apply(&[
        &Operation::Anti().apply(&[&Operation::IdDiff().apply(&[&a_next])]),
        &a_curr,
    ]);

    let w_b = Operation::TTranslate(-b).apply(&[&wp]);
    let b_prev = Operation::Prev(p).apply(&[&w_b]);
    let b_curr =
        Operation::SDilate(2.).apply(&[&Operation::CurrSeg(np).apply(&[&w_b])]);
    let rc_plot = Operation::Min()
        .apply(&[&Operation::IdDiff().apply(&[&b_prev]), &b_curr]);

    let m1 = Operation::Max().apply(&[&lm_plot, &rm_plot]);
    let m2 = Operation::Max().apply(&[&lc_plot, &rc_plot]);

    Operation::Max().apply(&[&m1, &m2])
}
