use crate::{plot::operation::Operation, signal::Signal, stl::stl::B};

use super::val_of;

#[must_use]
/// Algorithm for computing d(ω, φ) where φ = G_\[a, b\](p)
pub fn algo10(w: &Signal, a: f32, b: f32, p: &B) -> f32 {
    let np = val_of(&p.negate());
    let w_plot = w.to_plot(p.var());
    let p = val_of(p);
    let ab2 = (a + b) / 2.;
    let lm = Operation::Prev(np)
        .apply(&[&w_plot])
        .value_at(ab2)
        .unwrap_or_else(|| panic!("{ab2} is not a valid moment"))
        - a;
    let rm = b - Operation::Next(np)
        .apply(&[&w_plot])
        .value_at(ab2)
        .unwrap_or_else(|| panic!("{ab2} is not a valid moment"));
    let curr_seg = Operation::CurrSeg(np).apply(&[&w_plot]);
    let lc = f32::min(
        Operation::Next(p)
            .apply(&[&w_plot])
            .value_at(a)
            .unwrap_or_else(|| panic!("{a} is not a valid moment"))
            - a,
        curr_seg
            .value_at(a)
            .unwrap_or_else(|| panic!("{a} is not a valid moment"))
            / 2.,
    );
    let rc = f32::min(
        b - Operation::Prev(p)
            .apply(&[&w_plot])
            .value_at(b)
            .unwrap_or_else(|| panic!("{b} is not a valid moment")),
        curr_seg
            .value_at(b)
            .unwrap_or_else(|| panic!("{b} is not a valid moment"))
            / 2.,
    );
    let ma = |elems: &[_]| {
        *elems
            .iter()
            .max_by(|arg0: _, other: _| f32::total_cmp(arg0, other))
            .unwrap()
    };
    if [lc, rc] == [f32::INFINITY, f32::INFINITY] {
        ma(&[lm, rm])
    } else {
        ma(&[lm, rm, lc, rc])
    }
}
