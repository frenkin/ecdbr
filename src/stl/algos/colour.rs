use std::collections::HashMap;

use crate::signal::algorithms::range_sort::RangeUnion;
use crate::signal::values::IntValues;
use crate::stl::stl::B;
use crate::{
    signal::{algorithms::range_sort::range_sort, Signal},
    stl::stl::StlR,
};

/// Colouring algorithm.
#[must_use]
pub fn colour(w: &Signal, phi: &StlR, bis: &[&B]) -> (Signal, StlR) {
    let ranges = range_sort(w);
    let n = bis.len();
    let nb_vals = usize::pow(2, n.try_into().unwrap());
    let mut s: Vec<_> = (0..nb_vals).map(|_| RangeUnion::empty()).collect();
    for item in ranges.items() {
        let v = item.values();
        let mut b: usize = 0;
        bis.iter().enumerate().for_each(|(i, c)| {
            let bi = usize::from(c.eval(v));
            b |= bi << i;
        });
        for x in item.ranges().ranges() {
            s[b].ranges_mut().push(x.clone());
        }
    }

    let mut ranges = vec![];
    for (pos, ru) in s.iter().enumerate() {
        let x = IntValues::new(n, pos);
        for r in ru.ranges() {
            ranges.push((r, x));
        }
    }

    ranges.sort_by(|x, y| f32::partial_cmp(&x.0.start, &y.0.start).unwrap());
    let new_signal = Signal::from(ranges);

    let mut new_assign = HashMap::with_capacity(n);
    for (i, bi) in bis.iter().enumerate() {
        new_assign.insert(*bi, i);
    }
    let pp = phi.clone().replace(&new_assign);
    (new_signal, pp)
}
