use crate::{
    plot::{operation::Operation, Plot},
    signal::Signal,
    stl::stl::B,
};

use super::val_of;

#[must_use]
/// Algorithm for computing t ↦ d(ω, φ) where φ = G_\[a, t+b\](p)
pub fn algo14(w: &Signal, a: f32, b: f32, p: &B) -> Plot {
    let wp = w.to_plot(p.var());
    let np = val_of(&p.negate());
    let p = val_of(p);
    let w_m = Operation::TTranslate(-(a + b))
        .apply(&[&Operation::TDilate(2.).apply(&[&wp])]);
    let m_prev = Operation::Prev(np).apply(&[&w_m]);
    let m_prev = Operation::STranslate(a + b).apply(&[&m_prev]);
    let m_prev = Operation::SDilate(2.).apply(&[&m_prev]);
    let lm_plot = Operation::STranslate(-a).apply(&[&m_prev]);

    let m_next = Operation::Next(np).apply(&[&w_m]);
    let m_next = Operation::STranslate(a + b).apply(&[&m_next]);
    let m_next = Operation::SDilate(2.).apply(&[&m_next]);
    let rm_plot = Operation::STranslate(b)
        .apply(&[&Operation::IdDiff().apply(&[&m_next])]);

    let lc_plot = Operation::Const(f32::min(
        Operation::Next(p)
            .apply(&[&wp])
            .value_at(a)
            .unwrap_or_else(|| panic!("{a} is not a valid moment"))
            - a,
        Operation::CurrSeg(np)
            .apply(&[&wp])
            .value_at(a)
            .unwrap_or_else(|| panic!("{a} is not a valid moment"))
            / 2.,
    ))
    .apply(&[]);

    let w_b = Operation::TTranslate(-b).apply(&[&wp]);
    let b_prev = Operation::Prev(p).apply(&[&w_b]);
    let b_curr =
        Operation::SDilate(2.).apply(&[&Operation::CurrSeg(np).apply(&[&w_b])]);
    let rc_plot = Operation::Min()
        .apply(&[&Operation::IdDiff().apply(&[&b_prev]), &b_curr]);

    let m1 = Operation::Max().apply(&[&lm_plot, &rm_plot]);
    let m2 = Operation::Max().apply(&[&lc_plot, &rc_plot]);

    Operation::Max().apply(&[&m1, &m2])
}
