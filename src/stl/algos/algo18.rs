use crate::{
    plot::operation::Operation,
    signal::Signal,
    stl::{
        algos::{algo13::algo13, algo11::algo11},
        stl::B,
    },
};

/// Algorithm for computing d(ω, φ) where φ = ¬(G(p) ⇒ F_\[0, b\](q)).
#[must_use]
pub fn algo18(w: &Signal, p: &B, q: &B, b: f32) -> f32 {
    let p_plot = algo11(w, p);

    let neg_q = q.negate();
    let g_plot = algo13(w, 0., b, &neg_q);
    let d_plot = Operation::Max().apply(&[&p_plot, &g_plot]);

    let t = w.segments().iter().last().unwrap().high();
    d_plot.min(0., t).unwrap_or(0.)
}
