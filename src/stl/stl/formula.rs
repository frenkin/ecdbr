use core::ops::Deref;

use pyo3::{pyclass, pymethods};

use super::{StlR, B};

#[pyclass]
/// Python binding of [`StlR`].
#[derive(Debug)]
pub struct Formula {
    /// Formula contained.
    pub stl_r: StlR,
}

#[pymethods]
impl Formula {
    #[staticmethod]
    #[must_use]
    /// Creates a new formula representing a variable.
    pub fn var(v: usize) -> Self {
        Self {
            stl_r: StlR::B(Box::new(B::P(v))),
        }
    }

    #[staticmethod]
    #[must_use]
    /// Create a new formula representing the disjunction of two formulas.
    pub fn disj(f1: &Self, f2: &Self) -> Self {
        match (f1.stl_r.clone(), f2.stl_r.clone()) {
            (StlR::B(l), StlR::B(r)) => Self {
                stl_r: StlR::B(Box::new(B::Disj(l, r))),
            },
            (l, r) => Self {
                stl_r: StlR::Disj(Box::new(l), Box::new(r)),
            },
        }
    }

    #[staticmethod]
    #[must_use]
    /// Create a new formula representing the conjunction of two formulas.
    pub fn conj(f1: &Self, f2: &Self) -> Self {
        Self::neg(&Self::disj(&Self::neg(f1), &Self::neg(f2)))
    }

    #[staticmethod]
    #[must_use]
    /// Create an Until formula.
    pub fn until(start: f32, end: f32, b1: &Self, b2: &Self) -> Self {
        match (b1.stl_r.clone(), b2.stl_r.clone()) {
            (StlR::B(l), StlR::B(r)) => Self {
                stl_r: StlR::U(start..end, l, r),
            },
            _ => unreachable!(),
        }
    }

    #[staticmethod]
    #[must_use]
    /// Creates a Globally formula.
    pub fn g(start: f32, end: f32, b: &Self) -> Self {
        match b.stl_r.clone() {
            StlR::B(b) => Self {
                stl_r: StlR::G(start..end, b),
            },
            _ => unreachable!(),
        }
    }

    #[staticmethod]
    #[must_use]
    /// Creates a bounded response formula.
    pub fn gfi(end: f32, l: &Self, r: &Self) -> Self {
        match (l.stl_r.clone(), r.stl_r.clone()) {
            (StlR::B(l), StlR::B(r)) => Self {
                stl_r: StlR::GFI(l, end, r),
            },
            _ => unreachable!(),
        }
    }

    #[staticmethod]
    #[must_use]
    /// Negates a formula.
    pub fn neg(f: &Self) -> Self {
        match f.stl_r.clone() {
            StlR::B(b) => Self {
                stl_r: StlR::B(Box::new(B::Neg(b))),
            },
            f @ (StlR::G(..)
            | StlR::U(..)
            | StlR::GFI(..)
            | StlR::Neg(_)
            | StlR::Disj(..)) => Self {
                stl_r: StlR::Neg(Box::new(f)),
            },
        }
    }

    /// Function for displaying in a Python notebook.
    fn _repr_html_(&self) -> String {
        self.stl_r.to_string()
    }
}

impl Deref for Formula {
    type Target = StlR;

    fn deref(&self) -> &Self::Target {
        &self.stl_r
    }
}
