//! Algorithms used to compute [d](d::d).

use super::stl::B;
pub mod algo10;
pub mod algo11;
pub mod algo12;
pub mod algo13;
pub mod algo14;
pub mod algo15;
pub mod algo16;
pub mod algo17;
pub mod algo18;
pub mod algo4;
pub mod algo8;
pub mod algo9;
pub mod colour;
pub mod d;
pub mod python;
pub mod verify;
pub mod algo6;

/// Converts a B into a f32 between 0 and 1 (for plots).
#[must_use]
pub fn val_of(p: &B) -> f32 {
    match p {
        B::Neg(x) => {
            assert!(!matches!(**x, B::Neg(_)));
            0.
        },
        B::P(_) => 1.,
        _ => unreachable!(),
    }
}

