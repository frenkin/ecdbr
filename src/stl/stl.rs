pub mod formula;

use core::{fmt::Debug, ops::Range};
use std::collections::HashMap;

use crate::signal::values::IntValues;

#[derive(Debug, PartialEq, PartialOrd, Eq, Hash, Clone)]
/// Boolean Formula used in [`StlR`].
pub enum B {
    /// Variable
    P(usize),
    /// Disjunction
    Disj(Box<B>, Box<B>),
    /// Negation
    Neg(Box<B>),
}

#[derive(Clone, Debug)]
/// `STLᵣ` formula.
pub enum StlR {
    /// Boolean formula.
    B(Box<B>),
    /// Globally.
    G(Range<f32>, Box<B>),
    /// Until.
    U(Range<f32>, Box<B>, Box<B>),
    /// Bounded response.
    GFI(Box<B>, f32, Box<B>),
    /// Negation.
    Neg(Box<StlR>),
    /// Disjunction.
    Disj(Box<StlR>, Box<StlR>),
}

impl B {
    /// Returns the negation of this [`B`].
    ///
    /// This function should be used because it avoids double negations.
    #[must_use]
    pub fn negate(&self) -> Self {
        match self {
            B::Neg(b) => *b.clone(),
            B::P(_) | B::Disj(..) => Self::Neg(self.clone().into()),
        }
    }

    #[must_use]
    /// Checks if a valuation satisfies the formula.
    pub fn eval(&self, val: &IntValues) -> bool {
        match self {
            Self::P(pos) => val.bits()[*pos] == 1,
            Self::Disj(l, r) => l.eval(val) || r.eval(val),
            Self::Neg(b) => !b.eval(val),
        }
    }

    /// This function only handles cases that occur with
    /// [colour](crate::stl::algos::colour).
    #[must_use]
    pub fn replace(&self, new_assign: &HashMap<&Self, usize>) -> Self {
        new_assign
            .get(&self)
            .map_or_else(|| self.clone(), |val| Self::P(*val))
    }

    #[must_use]
    /// Variable of a formula.
    pub fn var(&self) -> usize {
        match self {
            B::P(p) => *p,
            B::Disj(b, b1) => {
                let l = b.var();
                let r = b1.var();
                assert!(l == r, "Should not be used on disjunctions");
                l
            },
            B::Neg(b) => b.var(),
        }
    }
}

impl StlR {
    /// Replace subformulas in self by values contained in `new_assign`.
    #[must_use]
    pub fn replace(&self, new_assign: &HashMap<&B, usize>) -> Self {
        match self {
            Self::B(v) => Self::B(Box::new(v.replace(new_assign))),
            Self::G(r, b) => Self::G(r.clone(), b.replace(new_assign).into()),
            Self::U(r, b1, b2) => Self::U(
                r.clone(),
                b1.replace(new_assign).into(),
                b2.replace(new_assign).into(),
            ),
            Self::GFI(b1, b, b2) => Self::GFI(
                b1.replace(new_assign).into(),
                *b,
                b2.replace(new_assign).into(),
            ),
            Self::Neg(p) => Self::Neg(p.replace(new_assign).into()),
            Self::Disj(l, r) => Self::Disj(
                l.replace(new_assign).into(),
                r.replace(new_assign).into(),
            ),
        }
    }

    /// Export a formula.
    #[must_use]
    #[allow(clippy::inherent_to_string)]
    pub fn to_string(&self) -> String {
        match self {
            Self::B(b) => match **b {
                B::P(x) => {
                    format!("p<sub>{x}</sub>")
                },
                B::Disj(ref l, ref r) => {
                    let l = Self::B(l.clone());
                    let r = Self::B(r.clone());
                    format!("({} \u{2228} {})", l.to_string(), r.to_string())
                },
                B::Neg(ref b) => {
                    format!("\u{ac}{}", Self::B(b.clone()).to_string())
                },
            },
            Self::G(r, b) => {
                format!(
                    "\u{25a1}<sub>[{}, {}]</sub>{}",
                    r.start,
                    r.end,
                    Self::B(b.clone()).to_string()
                )
            },
            Self::U(r, f1, f2) => {
                format!(
                    "({}U<sub>[{},{}]</sub> {})",
                    Self::B(f1.clone()).to_string(),
                    r.start,
                    r.end,
                    Self::B(f2.clone()).to_string()
                )
            },
            Self::GFI(f1, end, f2) => {
                format!(
                    "\u{25a1}({} \u{21d2} \u{2662}<sub>[0,{}]</sub> {})",
                    Self::B(f1.clone()).to_string(),
                    end,
                    Self::B(f2.clone()).to_string(),
                )
            },
            Self::Neg(f) => {
                format!("\u{ac}{}", f.to_string())
            },
            Self::Disj(l, r) => {
                format!("({} \u{2228} {})", l.to_string(), r.to_string())
            },
        }
    }

    #[must_use]
    /// Creates a Variable.
    pub fn var(p: usize) -> Self {
        Self::B(B::P(p).into())
    }

    #[must_use]
    /// Creates a Globally.
    pub fn g(a: f32, b: f32, f: Self) -> Self {
        if let Self::B(bi) = f {
            Self::G(a..b, bi)
        } else {
            unreachable!()
        }
    }

    #[must_use]
    /// Creates an Until.
    pub fn until(a: f32, b: f32, b1: Self, b2: Self) -> Self {
        match (b1, b2) {
            (Self::B(bi1), Self::B(bi2)) => Self::U(a..b, bi1, bi2),
            _ => unreachable!(),
        }
    }

    #[must_use]
    /// Creates a bounded response.
    pub fn gfi(b: f32, b1: Self, b2: Self) -> Self {
        match (b1, b2) {
            (Self::B(bi1), Self::B(bi2)) => Self::GFI(bi1, b, bi2),
            _ => unreachable!(),
        }
    }

    #[allow(clippy::should_implement_trait)]
    #[must_use]
    /// Negates a formula.
    ///
    /// This function must be used to avoid double negation.
    pub fn not(self) -> Self {
        match self {
            Self::B(b) => match *b {
                B::P(_) => Self::B(Box::new(B::Neg(b))),
                x @ B::Disj(..) => Self::Neg(Box::new(Self::B(Box::new(x)))),
                B::Neg(_) => Self::B(b),
            },
            x => Self::Neg(Box::new(x)),
        }
    }

    #[must_use]
    /// Creates a disjunction of two formulas.
    pub fn disj(f1: Self, f2: Self) -> Self {
        match (f1, f2) {
            (Self::B(b1), Self::B(b2)) => Self::B(Box::new(B::Disj(b1, b2))),
            (f1, f2) => Self::Disj(Box::new(f1), Box::new(f2)),
        }
    }
}
