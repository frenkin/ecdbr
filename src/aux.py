def display_signal(signal):
  import numpy as np
  import matplotlib.pyplot as plt
  import math

  last_moment = math.ceil(signal.segments()[-1].high())
  # Dimension of the signal
  m = signal.dimension()
  n = 10000

  # Set of x's used for drawing
  x = np.linspace(0, last_moment, n)

  limits = list()
  for segment in signal.segments():
    limits.append(segment.low())
  limits.append(segment.high())
  limits = np.array(limits)

  x = np.unique(np.concatenate((x, limits)))

  # Values of the signal
  y = np.zeros((m, n))
  for i in range(n):
    y[:, i] = signal.segment_at(x[i]).py_values().bits()

  _, axs = plt.subplots(m, 1, figsize=(12, 1.5 * m))

  if m == 1:
    axs = [axs]

  for i in range(m):
    for segment in signal.segments():
      mask = (x > segment.low()) & (x < segment.high())
      if segment.py_values().bits()[i] == 1:
        color = "red"
      else:
        color = "blue"
      axs[i].step(x[mask], segment.py_values().bits()[i] * np.ones_like(x[mask]), color=color, linewidth=4)
    axs[i].set_xlabel('$t$')
    axs[i].set_xticks(sorted(limits))
    axs[i].tick_params(axis='x', direction='inout', length=8)
    axs[i].set_xlim(left=0)
    axs[i].set_ylabel(f'$p_{i}(t)$')
    axs[i].set_yticks([0, 1])
    axs[i].set_ylim(bottom=0, top=1)
    axs[i].tick_params(axis='y', direction='inout', length=8)
    axs[i].spines['right'].set_visible(False)
    axs[i].spines['top'].set_visible(False)

    labels = axs[i].get_xticks()
    for j, label in enumerate(labels):
        if j == 0:
           continue
        if j % 2 == 0:
            axs[i].text(label, -0.05, str(round(label, 2)), transform=axs[i].get_xaxis_transform(), ha='center', va='top', rotation=45)
        else:
            axs[i].text(label, 0.05, str(round(label, 2)), transform=axs[i].get_xaxis_transform(), ha='center', va='bottom', rotation=45)
    axs[i].set_xticklabels([])

  plt.tight_layout()
  plt.show()

def display_plot(plot):
    import matplotlib.pyplot as plt
    import numpy as np
    import math

    def valof(x):
        return plot.value_at(x)

    for segment in plot.segments():
        if np.isinf(segment.low()):
           low = -20
        else:
           low = segment.low()
        if np.isinf(segment.high()):
          high = 20
        else:
          high = segment.high()
        x = np.linspace(low, high, 100, endpoint=False)
        y = list(map(valof, x))
        plt.plot(x, y, color = "blue", linewidth=2)
    plt.show()
