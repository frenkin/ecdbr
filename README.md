# ECDBR

This repository contains the source files for the ecdbr tool, as well as
notebooks showing how to use it. A Dockerfile is also present.

## Compilation

use `rustup default nightly` to switch to the "nightly" version.

Use `maturin develop` to compile.

Compile STLRom in the associated folder, following the README.

## Server for notebooks

Use `jupyter-lab --port=8891 --allow-root --no-browser --ip=0.0.0.0`

A line starting with http://127.0.0.1:8891/lab?token= should appear.

Open this link in your browser.

Remark: The `ecdbr` kernel must be used.

## Docker

### Create an image

Use `docker build -t ec .` to create an image…

### Use existing

…or use `docker load < ec.tar`

### Run the image

Use `docker run -p 8891:8891 -it ec`

## Files provided

* notebooks/eval_ex.ipynb: Notebook to reproduce the results presented.
* notebooks/example.ipynb: Notebook showing how to use the tool.
